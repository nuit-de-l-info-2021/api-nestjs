FROM node:14-alpine AS build-env

WORKDIR /app
COPY ./package*.json ./
RUN npm ci --prod
COPY ./dist .

FROM node:14-alpine

WORKDIR /app
COPY --from=build-env /app .

EXPOSE 3000
CMD ["main.js"]
