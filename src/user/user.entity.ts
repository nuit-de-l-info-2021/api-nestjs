import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import {
  IsBoolean,
  IsEmail,
  IsOptional,
  IsString,
  MaxLength,
} from 'class-validator';
import { Exclude } from 'class-transformer';
import { ApiHideProperty } from '@nestjs/swagger';

import { UserToken } from './token/user-token.entity';

@Entity('user')
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @IsString()
  @MaxLength(50)
  @IsOptional()
  @Column('varchar', { length: 50, unique: true })
  username: string;

  @IsString()
  @MaxLength(50)
  @IsOptional()
  @ApiHideProperty()
  @Column('varchar', { length: 250, unique: true })
  password: string;

  @IsEmail()
  @MaxLength(200)
  @IsOptional()
  @Column('varchar', { length: 200, nullable: true })
  mail: string;

  @IsBoolean()
  @IsOptional()
  @Column('boolean', { default: false })
  isAdmin?: boolean = false;

  @OneToMany(() => UserToken, (token) => token.userOwner, {
    cascade: true,
  })
  @Exclude()
  tokens?: UserToken[];
}
