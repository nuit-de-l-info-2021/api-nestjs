import { Body, Controller, Delete, Get, Param, Patch } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';

import { UserService } from './user.service';
import { CrudController } from '../crud/crud.controller';
import { User } from './user.entity';

const ENTITY = 'user';

@ApiBearerAuth()
@ApiTags(ENTITY)
@Controller(ENTITY)
export class UserController extends CrudController<User> {
  constructor(private readonly userService: UserService) {
    super(userService);
  }

  @Get()
  async findAll(): Promise<User[]> {
    return this.userService.findAll();
  }

  @Get(':id')
  async findById(@Param('id') id: string): Promise<User> {
    return this.userService.findOneOrFailById(id);
  }

  @Patch(':id')
  async update(@Param('id') id: string, @Body() user: User): Promise<User> {
    return this.userService.update({ id, ...user });
  }

  @Delete(':id')
  async delete(@Param('id') id: string) {
    await this.findById(id);
    return await this.userService.delete(id);
  }
}
