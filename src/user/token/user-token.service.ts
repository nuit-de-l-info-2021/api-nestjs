import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcrypt';
import { CrudService } from '../../crud/crud.service';
import { LessThanOrEqual } from 'typeorm';

import { UserToken } from './user-token.entity';

@Injectable()
export class UserTokenService extends CrudService<UserToken> {
  constructor(@InjectRepository(UserToken) repo) {
    super(repo);
  }

  public async createWithUser(userId: string): Promise<UserToken> {
    const userToken: UserToken = {
      token: '',
      expire: new Date(),
      userOwner: { id: userId },
    } as UserToken;
    return await this.create(userToken);
  }

  private async getUserTokensByUserOwner(userId: string): Promise<UserToken[]> {
    return await this.findAll({ where: { userOwner: { id: userId } } });
  }

  public async updateToken(userToken: UserToken): Promise<UserToken> {
    userToken.token = await bcrypt.hash(userToken.token, 10);
    return await this.update(userToken);
  }

  public async getFromUser(token: string, userId: string): Promise<UserToken> {
    const tokenList: Array<UserToken> = await this.getUserTokensByUserOwner(
      userId,
    );
    let matchedToken: UserToken = null;
    for (const userToken of tokenList) {
      const match: boolean = await bcrypt.compare(token, userToken.token);
      if (match) {
        matchedToken = userToken;
      }
    }
    return matchedToken;
  }

  public async deleteToken(tokenId: string): Promise<void> {
    await this.delete({ id: tokenId });
  }

  public async deleteExpiredTokens(): Promise<void> {
    await this.delete({ expire: LessThanOrEqual(new Date()) });
  }
}
