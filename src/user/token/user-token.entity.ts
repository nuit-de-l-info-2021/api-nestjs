import { IsNotEmpty, IsString, MaxLength } from 'class-validator';
import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { User } from '../user.entity';

@Entity('user_token')
export class UserToken {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @IsString()
  @IsNotEmpty()
  @MaxLength(60)
  @Column('varchar', { length: 60 })
  token: string;

  @Column({ type: 'timestamp' })
  expire: Date;

  @ManyToOne(() => User, (user) => user.tokens, {
    onDelete: 'CASCADE',
  })
  userOwner: User;
}
