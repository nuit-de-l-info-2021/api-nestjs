import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { IsInt, IsOptional, IsString } from 'class-validator';

@Entity('lifeguard')
export class Lifeguard {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @IsString()
  @IsOptional()
  @Column('varchar', { length: 100 })
  name?: string;

  @IsString()
  @IsOptional()
  @Column('varchar', { length: 100, nullable: true })
  surname?: string;

  @IsInt()
  @IsOptional()
  @Column('integer', { default: 0 })
  numberOfRescues?: number;

  @IsInt()
  @IsOptional()
  @Column('integer', { default: 0 })
  numberOfMedal?: number;

  @IsString()
  @IsOptional()
  @Column('varchar', { length: 250, nullable: true })
  imageUrl?: string;

  @IsOptional()
  @Column('text', { nullable: true })
  description?: string;
}
