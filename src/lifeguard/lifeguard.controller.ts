import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';

import { CrudController } from '../crud/crud.controller';
import { AuthRoleScope, AuthScope } from '../auth/auth.guard';
import { Lifeguard } from './lifeguard.entity';
import { LifeguardService } from './lifeguard.service';

const ENTITY = 'lifeguard';

@ApiTags(ENTITY)
@Controller(ENTITY)
export class LifeguardController extends CrudController<Lifeguard> {
  constructor(private readonly lifeguardService: LifeguardService) {
    super(lifeguardService);
  }

  @AuthScope(AuthRoleScope.Public)
  @Get()
  async findAll(): Promise<Lifeguard[]> {
    return this.lifeguardService.findAll();
  }

  @AuthScope(AuthRoleScope.Public)
  @Get(':id')
  async findById(@Param('id') id: string): Promise<Lifeguard> {
    return this.lifeguardService.findOneOrFailById(id);
  }

  @AuthScope(AuthRoleScope.Admin)
  @ApiBearerAuth()
  @Post()
  async create(@Body() lifeguard: Lifeguard): Promise<Lifeguard> {
    return this.lifeguardService.create(lifeguard);
  }

  @AuthScope(AuthRoleScope.Admin)
  @ApiBearerAuth()
  @Patch(':id')
  async update(
    @Param('id') id: string,
    @Body() lifeguard: Lifeguard,
  ): Promise<Lifeguard> {
    return this.lifeguardService.update({ id, ...lifeguard });
  }

  @AuthScope(AuthRoleScope.Admin)
  @ApiBearerAuth()
  @Delete(':id')
  async delete(@Param('id') id: string) {
    await this.findById(id);
    return await this.lifeguardService.delete(id);
  }
}
