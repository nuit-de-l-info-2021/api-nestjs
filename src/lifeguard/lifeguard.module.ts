import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { Lifeguard } from './lifeguard.entity';
import { LifeguardService } from './lifeguard.service';
import { LifeguardController } from './lifeguard.controller';

@Module({
  imports: [TypeOrmModule.forFeature([Lifeguard])],
  providers: [LifeguardService],
  controllers: [LifeguardController],
  exports: [],
})
export class LifeguardModule {}
