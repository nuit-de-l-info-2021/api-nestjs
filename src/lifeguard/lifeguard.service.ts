import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { CrudService } from '../crud/crud.service';
import { Lifeguard } from './lifeguard.entity';

@Injectable()
export class LifeguardService extends CrudService<Lifeguard> {
  constructor(
    @InjectRepository(Lifeguard) lifeguardRepository: Repository<Lifeguard>,
  ) {
    super(lifeguardRepository);
  }
}
