import * as cls from 'cls-hooked';
import { Request, Response } from 'express';

export class RequestContext {
  readonly id: number;
  request: Request;
  response: Response;

  constructor(request: Request, response: Response) {
    this.id = Math.random();
    this.request = request;
    this.response = response;
  }

  static currentRequestContext(): RequestContext {
    const session = cls.getNamespace(RequestContext.name);
    if (session && session.active) {
      return session.get(RequestContext.name);
    }

    return null;
  }

  static currentRequest(): Request {
    const requestContext = RequestContext.currentRequestContext();

    if (requestContext) {
      return requestContext.request;
    }

    return null;
  }

  static currentUserId(): string {
    const user: any = RequestContext.currentUser();
    if (user) {
      return user.id;
    }
    return null;
  }

  static currentUser(): any {
    const requestContext = RequestContext.currentRequestContext();

    if (requestContext) {
      const user: any = requestContext.request['user'];

      if (user) {
        return user;
      }
    }

    return null;
  }
}
