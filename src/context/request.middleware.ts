import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response } from 'express';
import * as cls from 'cls-hooked';

import { RequestContext } from './request-context';

@Injectable()
export class RequestMiddleware implements NestMiddleware<Request, Response> {
  use(req: Request, res: Response, next: () => void) {
    const requestContext = new RequestContext(req, res);
    const session =
      cls.getNamespace(RequestContext.name) ||
      cls.createNamespace(RequestContext.name);

    session.run(async () => {
      session.set(RequestContext.name, requestContext);
      next();
    });
  }
}
