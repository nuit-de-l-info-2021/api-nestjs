import { ClassSerializerInterceptor, ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory, Reflector } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import * as cookieParser from 'cookie-parser';

import { AppModule } from './app.module';
import getLogLevels from './utils/log-levels';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    cors: true,
    logger: getLogLevels(process.env.NODE_ENV === 'production'),
  });
  const configService: ConfigService = app.get(ConfigService);

  const APP_NAME = process.env.npm_package_name;
  const APP_VERSION = process.env.npm_package_version;

  app.use(cookieParser());
  app.setGlobalPrefix('api');
  app.useGlobalPipes(new ValidationPipe());
  app.useGlobalInterceptors(new ClassSerializerInterceptor(app.get(Reflector)));

  const swgOptions = new DocumentBuilder()
    .setTitle(APP_NAME)
    .setDescription(`${APP_NAME} API description`)
    .setVersion(APP_VERSION)
    .addBearerAuth()
    .build();
  const swgDoc = SwaggerModule.createDocument(app, swgOptions);
  SwaggerModule.setup('api-docs', app, swgDoc);

  const SVC_PORT = configService.get<number>('SVC_PORT');
  await app.listen(SVC_PORT);
}
bootstrap();
