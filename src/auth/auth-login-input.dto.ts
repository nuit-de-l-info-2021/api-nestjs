import { IsNotEmpty, IsString, MaxLength } from 'class-validator';

export class AuthLoginInput {
  @IsString()
  @IsNotEmpty()
  @MaxLength(50)
  username: string;

  @IsString()
  @IsNotEmpty()
  @MaxLength(50)
  password: string;
}
