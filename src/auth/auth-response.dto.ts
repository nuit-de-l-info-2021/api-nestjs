export class AuthResponse {
  id: string;
  username: string;
  mail: string;
  isAdmin: boolean;
  token: string;
  tokenExpire: Date;
}
