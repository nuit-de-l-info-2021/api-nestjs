import { IsEmail, IsNotEmpty, IsOptional, IsString, MaxLength } from 'class-validator';

export class AuthRegisterInput {
  @IsString()
  @IsNotEmpty()
  @MaxLength(50)
  username: string;

  @IsString()
  @IsNotEmpty()
  @MaxLength(50)
  password: string;

  @IsEmail()
  @IsOptional()
  @MaxLength(50)
  mail: string;
}
