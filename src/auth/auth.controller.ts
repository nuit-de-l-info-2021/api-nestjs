import {
  Controller,
  UnauthorizedException,
  Get,
  Post,
  Req,
  Res,
  Body,
  Logger,
} from '@nestjs/common';
import { ApiBearerAuth, ApiCookieAuth, ApiTags } from '@nestjs/swagger';
import * as bcrypt from 'bcrypt';
import { ConfigService } from '@nestjs/config';


import { User } from '../user/user.entity';
import { Request, Response } from 'express';
import { AuthResponse } from './auth-response.dto';
import { AuthService } from './auth.service';
import { AuthRoleScope, AuthScope } from './auth.guard';
import { AuthLoginInput } from './auth-login-input.dto';
import { AuthRegisterInput } from './auth-register-input.dto';
import { UserService } from '../user/user.service';

const ENTITY = 'auth';

@ApiTags(ENTITY)
@Controller(ENTITY)
export class AuthController {
  private readonly logger = new Logger(AuthController.name);
  private saltRounds: number;
  constructor(
    private readonly authService: AuthService,
    private readonly userService: UserService,
    private readonly configService: ConfigService,
  ) {
    this.saltRounds = parseInt(this.configService.get('BCRYPT_SALT_ROUNDS'));
  }

  @AuthScope(AuthRoleScope.Public)
  @Post('/login')
  async login(@Body() authLoginInput: AuthLoginInput): Promise<AuthResponse> {
    const user = await this.userService.getUserByUsername(
      authLoginInput.username,
    );
    if (
      !user ||
      !(await bcrypt.compare(authLoginInput.password, user.password))
    ) {
      return null;
    }

    await this.authService.deleteExpiredRefreshTokens();
    return this.authService.getAuthUser(user);
  }

  @AuthScope(AuthRoleScope.Public)
  @Post('/register')
  async register(
    @Body() authRegisterInput: AuthRegisterInput,
  ): Promise<AuthResponse> {
    this.logger.debug('Register ', authRegisterInput);

    const salt = await bcrypt.genSalt(this.saltRounds);

    const hash = await bcrypt.hash(authRegisterInput.password, salt);

    delete authRegisterInput.password // fixed ..

    const user = await this.userService.create({
      ...authRegisterInput,
      password: hash,
    });
    return this.authService.getAuthUser(user);
  }

  @ApiCookieAuth()
  @ApiBearerAuth()
  @Post('refresh')
  async refresh(
    @Req() req: Request,
    @Res({ passthrough: true }) res: Response,
  ) {
    await this.authService.deleteExpiredRefreshTokens();
    const refreshToken: string = req.cookies['auth-token-refresh'];
    const user: User = (req as any).user;
    if (refreshToken) {
      const userToken: any = await this.authService.validateRefreshToken(
        user,
        refreshToken,
      );
      if (userToken) {
        await this.setRefreshToken(req, res);
        await this.authService.deleteRefreshToken(userToken.id);
        return this.authService.getAuthUser(user);
      }
    }
    throw new UnauthorizedException();
  }

  @ApiBearerAuth()
  @Get('profile')
  async getProfile(@Req() req: Request): Promise<User> {
    const user = await this.authService.getUser((req as any).user.id);
    delete user.password; // fixed :)
    return user;
  }

  private async setRefreshToken(req: Request, res: Response) {
    const refreshToken: any = await this.authService.getRefreshToken(
      (req as any).user,
    );
    const urlPath: Array<string> = req.url.split('/');
    urlPath.pop();
    res.cookie('auth-token-refresh', refreshToken.token, {
      expires: refreshToken.expire,
      httpOnly: true,
      path: urlPath.join('/'),
      sameSite: 'strict',
    });
    return req['user'];
  }
}
