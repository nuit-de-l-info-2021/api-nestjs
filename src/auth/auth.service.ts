import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService, JwtSignOptions } from '@nestjs/jwt';

import { User } from '../user/user.entity';
import { UserService } from '../user/user.service';
import { UserToken } from '../user/token/user-token.entity';
import { UserTokenService } from '../user/token/user-token.service';
import { AuthResponse } from './auth-response.dto';

@Injectable()
export class AuthService {
  constructor(
    private configService: ConfigService,
    private userService: UserService,
    private userTokenService: UserTokenService,
    private jwtService: JwtService,
  ) {}

  public async getUser(id: string): Promise<User> {
    return await this.userService.findOneOrFailById(id);
  }

  public getAuthUser(user: any): AuthResponse {
    const payload: any = {
      id: user.id,
      username: user.username,
      mail: user.mail,
      isAdmin: user.isAdmin,
    };

    const { token, expire }: any = this.getToken(payload);
    return {
      id: user.id,
      username: user.username,
      mail: user.mail,
      isAdmin: user.isAdmin,
      token: token,
      tokenExpire: expire,
    } as AuthResponse;
  }

  public async getRefreshToken(user: User): Promise<any> {
    const tokenConf: any = this.configService.get<any>('auth.jwtRefresh');
    let userToken: UserToken = await this.userTokenService.createWithUser(
      user.id,
    );
    const payload: any = { id: userToken.id };
    const tokenInfo: any = this.getToken(payload, tokenConf);
    userToken.token = tokenInfo.token;
    userToken.expire = new Date(tokenInfo.expire);
    userToken = await this.userTokenService.updateToken(userToken);
    return { token: tokenInfo.token, expire: userToken.expire };
  }

  public async validateRefreshToken(
    user: User,
    token: string,
  ): Promise<UserToken> {
    const tokenInfo: any = this.jwtService.decode(token);
    const expire: number = tokenInfo.exp * 1000;
    if (expire < Date.now()) {
      return null;
    }
    const userToken: UserToken = await this.userTokenService.getFromUser(
      token,
      user.id,
    );
    return userToken;
  }

  public async deleteRefreshToken(tokenId: string): Promise<void> {
    await this.userTokenService.deleteToken(tokenId);
  }

  public async deleteExpiredRefreshTokens(): Promise<void> {
    await this.userTokenService.deleteExpiredTokens();
  }

  private getToken(payload: any, conf?: any): any {
    const signOptions: JwtSignOptions = conf
      ? { secret: conf.secret, expiresIn: conf.signOptions.expiresIn }
      : undefined;
    const token: string = this.jwtService.sign(payload, signOptions);
    const tokenInfo: any = this.jwtService.decode(token);
    const expire: number = tokenInfo.exp * 1000;
    return { token, expire };
  }
}
