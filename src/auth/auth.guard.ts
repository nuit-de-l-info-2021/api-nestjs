import {
  ExecutionContext,
  Injectable,
  UnauthorizedException,
  SetMetadata,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { AuthGuard as AuthGuardNative } from '@nestjs/passport';

export enum AuthRoleScope {
  Public = 'public',
  Admin = 'admin',
}

export const AUTH_SCOPE_KEY = 'AuthScope';
export const AuthScope = (...scopes: AuthRoleScope[]) =>
  SetMetadata(AUTH_SCOPE_KEY, scopes);

@Injectable()
export class AuthGuard extends AuthGuardNative('jwt') {
  constructor(private reflector: Reflector) {
    super();
  }

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const ctxClass: any = context.getClass();
    const ctxHandler: any = context.getHandler();

    const scopes: string[] =
      this.reflector.getAllAndOverride<AuthRoleScope[]>(AUTH_SCOPE_KEY, [
        ctxClass,
        ctxHandler,
      ]) || [];
    if (scopes.includes(AuthRoleScope.Public)) {
      return true;
    }

    const isLogged = await super.canActivate(context);
    if (!isLogged) {
      return false;
    }

    const request: any = context.switchToHttp().getRequest();
    const user = request.user;
    if (!user) {
      return false;
    }

    if (user.isAdmin === true) {
      return true;
    } else if (scopes.includes(AuthRoleScope.Admin)) {
      return false;
    }

    return true;
  }

  handleRequest(err, user) {
    if (err || !user) {
      throw err || new UnauthorizedException();
    }
    return user;
  }
}
