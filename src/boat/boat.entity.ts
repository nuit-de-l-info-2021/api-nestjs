import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { IsOptional, IsString } from 'class-validator';

@Entity('boat')
export class Boat {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @IsString()
  @IsOptional()
  @Column('varchar', { length: 200 })
  name: string;

  @IsString()
  @IsOptional()
  @Column('varchar', { length: 250, nullable: true })
  imageUrl?: string;

  @IsOptional()
  @Column('text', { nullable: true })
  description?: string;
}
