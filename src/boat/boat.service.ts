import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { CrudService } from '../crud/crud.service';
import { Boat } from './boat.entity';

@Injectable()
export class BoatService extends CrudService<Boat> {
  constructor(@InjectRepository(Boat) boatRepository: Repository<Boat>) {
    super(boatRepository);
  }
}
