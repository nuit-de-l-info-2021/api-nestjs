import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';

import { CrudController } from '../crud/crud.controller';
import { AuthRoleScope, AuthScope } from '../auth/auth.guard';
import { BoatService as BoatService } from './boat.service';
import { Boat } from './boat.entity';

const ENTITY = 'boat';

@ApiTags(ENTITY)
@Controller(ENTITY)
export class BoatController extends CrudController<Boat> {
  constructor(private readonly boatService: BoatService) {
    super(boatService);
  }

  @AuthScope(AuthRoleScope.Public)
  @Get()
  async findAll(): Promise<Boat[]> {
    return this.boatService.findAll();
  }

  @AuthScope(AuthRoleScope.Public)
  @Get(':id')
  async findById(@Param('id') id: string): Promise<Boat> {
    return this.boatService.findOneOrFailById(id);
  }

  @AuthScope(AuthRoleScope.Admin)
  @ApiBearerAuth()
  @Post()
  async create(@Body() boat: Boat): Promise<Boat> {
    return this.boatService.create(boat);
  }

  @AuthScope(AuthRoleScope.Admin)
  @ApiBearerAuth()
  @Patch(':id')
  async update(@Param('id') id: string, @Body() boat: Boat): Promise<Boat> {
    return this.boatService.update({ id, ...boat });
  }

  @AuthScope(AuthRoleScope.Admin)
  @ApiBearerAuth()
  @Delete(':id')
  async delete(@Param('id') id: string) {
    await this.findById(id);
    return await this.boatService.delete(id);
  }
}
