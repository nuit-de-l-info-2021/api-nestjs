import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { Boat } from './boat.entity';
import { BoatService } from './boat.service';
import { BoatController } from './boat.controller';

@Module({
  imports: [TypeOrmModule.forFeature([Boat])],
  providers: [BoatService],
  controllers: [BoatController],
  exports: [],
})
export class BoatModule {}
