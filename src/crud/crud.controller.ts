import { CrudService } from './crud.service';

export abstract class CrudController<T> {
  constructor(private readonly crudService: CrudService<T>) {}
}
