import { BadRequestException, NotFoundException } from '@nestjs/common';
import {
  DeepPartial,
  DeleteResult,
  FindConditions,
  FindManyOptions,
  FindOneOptions,
  Repository,
} from 'typeorm';

export abstract class CrudService<T> {
  constructor(protected readonly repository: Repository<T>) {}

  public async findAll(filter?: FindManyOptions<T>): Promise<T[]> {
    return await this.repository.find(filter);
  }

  public async findOneByConditions(
    conditions: FindConditions<T>,
    options?: FindOneOptions<T>,
  ): Promise<any> {
    return await this.repository.findOne(conditions, options);
  }

  public async findOneOrFailById(
    id: string,
    options?: FindOneOptions<T>,
  ): Promise<any> {
    return await this.repository.findOneOrFail(id, options);
  }

  async create(entity: DeepPartial<T>) {
    const obj = await this.repository.create(entity);
    try {
      const res = await this.repository.save(obj);
      return res;
    } catch (err) {
      throw new BadRequestException(err);
    }
  }

  public async update(partialEntity: DeepPartial<T>): Promise<T> {
    const entity = await this.repository.preload(partialEntity);
    return this.repository.save(entity);
  }

  public async delete(
    criteria: string | number | FindConditions<T>,
  ): Promise<DeleteResult> {
    try {
      return await this.repository.delete(criteria);
    } catch (error) {
      throw new NotFoundException(`The record was not found`, error);
    }
  }
}
